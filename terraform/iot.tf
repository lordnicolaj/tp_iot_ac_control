# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}

# aws_iot_policy pub-sub
resource "aws_iot_policy" "pubsub" {
  name = "PubSubToAnyTopic"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = file("./files/iot_policy.json")
}

# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "attachment" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}


# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "temp_sensor"
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "thing_attachment" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
    endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "high_temperature_rule" {
  name        = "TemperatureRule"
  description = "A rule to store high temperature readings"
  enabled     = true

  sql = "SELECT * FROM 'sensor/temperature/+' WHERE temperature >= 40"
  sql_version = "2016-03-23"  // Use the appropriate SQL version

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn // Assuming iot_role is already defined

    put_item {
      table_name = aws_dynamodb_table.temperature.name
    }
  }
}
# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "all_temperature_rule" {
  name        = "TemperatureToTimestreamRule"
  description = "Forward all temperature data to Timestream"
  enabled     = true

  sql = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    role_arn   = aws_iam_role.iot_role.arn
    database_name = aws_timestreamwrite_database.iot_db.database_name
    table_name = aws_timestreamwrite_table.temperature_sensor.table_name

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
