# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "iot_db" {
  database_name = "iot"
}

# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "temperature_sensor" {
  database_name = aws_timestreamwrite_database.iot_db.database_name
  table_name    = "temperaturesensor"
}
